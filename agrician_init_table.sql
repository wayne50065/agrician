CREATE TABLE users(
    id SERIAL                   primary key,
    username                    VARCHAR(255),
    password                    VARCHAR(255),
    is_super_user               integer
);

CREATE TABLE customers(
    id SERIAL                   primary key,
    users_id                    integer,
    FOREIGN KEY (users_id) REFERENCES users(id),
    address                     TEXT,
    email                       VARCHAR(255),
    date_of_birth               DATE,
    gender                      TEXT,
    mailing_list                INTEGER
);

CREATE TABLE statuses(
    id SERIAL                   primary key,
    detail                      TEXT
);

CREATE TABLE deliveries(
    id SERIAL                   primary key,
    delivery_method             TEXT,
    statuses_id                 INTEGER,
    FOREIGN KEY (statuses_id) REFERENCES statuses(id)
);

CREATE TABLE subscription_packages(
    id SERIAL                   primary key,
    name                        TEXT,
    description                 VARCHAR(255),
    price                       DECIMAL
);

CREATE TABLE subscriptions(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    deliver_id                  INTEGER,
    FOREIGN KEY (deliver_id) REFERENCES deliveries(id),
    start_date                  TIMESTAMP,
    end_date                    TIMESTAMP,
    next_payment_date           TIMESTAMP,
    frequency                   VARCHAR(255),
    charge                      DECIMAL
);

CREATE TABLE subsciptions_recurring_payments(
    id SERIAL                   primary key,
    subscription_id             INTEGER,
    FOREIGN KEY (subscription_id) REFERENCES subscriptions(id),
    period_start_date           TIMESTAMP,
    period_end_date             TIMESTAMP,
    charge_interval             VARCHAR(255),
    billing_address             VARCHAR(255),
    invoice                     VARCHAR(255)
);



CREATE TABLE subscription_batches(
    id SERIAL                   primary key,
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    signiture_items_id          INTEGER,
    FOREIGN KEY (signiture_items_id) REFERENCES signiture_items(id),
    launch_date                 TIMESTAMP,
    off_shelf_date              TIMESTAMP,
    is_active                   INTEGER
);



CREATE TABLE signiture_items(
    id SERIAL                   primary key,
    name                        VARCHAR(255),
    description                 VARCHAR(255),
    image                       VARCHAR(255)
);

CREATE TABLE products(
    id SERIAL                   primary key,
    name               VARCHAR(255),
    price                       DECIMAL,
    stock                       INTEGER,
    image                       TEXT,
    description                 TEXT,
    category_id                 INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);


CREATE TABLE subscription_items(
    id SERIAL                   primary key,
    product_id                  INTEGER,
    FOREIGN KEY (product_id) REFERENCES products(id),
    subscription_batches_id    INTEGER,
    FOREIGN KEY (subscription_batches_id) REFERENCES subscription_batches(id),
    quantity                    INTEGER
);

CREATE TABLE categories(
    id SERIAL                   primary key,
    category_name                        TEXT,
    category_description                 TEXT
);

CREATE TABLE carts(
    id SERIAL                   primary key,
    product_id                  INTEGER,
    customer_id                 INTEGER,
    FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE order_items(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    product_name                TEXT,
    price                       DECIMAL,
    quantity                    INTEGER
);

CREATE TABLE orders(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    date                        TIMESTAMP,
    total_price                 DECIMAL,
    delivery_id                 INTEGER
);

CREATE TABLE payments(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    order_id                    INTEGER,
    FOREIGN KEY (order_id) REFERENCES orders(id),
    date                        TIMESTAMP,
    amount                      DECIMAL,
    method                      TEXT,
    billing_address             TEXT,
    invoice                     TEXT
);