import express,{ Request, Response, NextFunction } from 'express';
import {client} from './app';
import {hashPassword} from './bcrypt';
import { User } from './models';
import { checkPassword } from './bcrypt';
import expressSession from 'express-session';


export const usersRoutes = express.Router();


usersRoutes.use(expressSession({
    secret: 'agrician',
    resave: true,
    saveUninitialized: true
}))

usersRoutes.use(express.json());
usersRoutes.use(express.urlencoded({extended:true}))





usersRoutes.post('/login',async (req,res)=>{
    const login_username=req.body.username    
    const login_password=req.body.password
    let users:User[]=(await client.query(`select * from users where username=$1`,
                        [login_username])).rows
    const user=users[0];
    if(user!=undefined && await user.is_super_user == 1 && await checkPassword(login_password,user.password)){
        req.session['user'] = user;
        req.session['admin'] = true;
        res.status(200).json({success:true});
    }
    else if(user!=undefined && await checkPassword(login_password,user.password)){
        req.session['user'] = user;
        res.status(200).json({success:true});
    }else{
        res.status(401).json({success:false});
    }
});



usersRoutes.post('/register',async (req,res)=>{
    const regist_username=req.body.username;
    const regist_password=req.body.password;
    const regist_password2=req.body.password2;

    
    
    const isUserExist=(await client.query(`select username from users where username=$1`,
                        [regist_username])).rows

    if(isUserExist[0]) {
        res.status(400).json({msg:"Username has been used"});
    } else if (regist_password2 != regist_password){
        res.status(401).json({msg:"Password not match!"})
    } else if (req.body.username==""){
        res.status(400).json({msg:"Please input your email and password."});
    }else{
    const bcryptPassword=await hashPassword(regist_password)
    await client.query(`insert into users (username, password, is_super_user) values ($1,$2,$3)`,
                        [regist_username,bcryptPassword,0]);
    res.status(200).json({msg:"Account created successfully! Welcome!"})}
})

usersRoutes.get('/current-user',async(req,res)=>{
    if(req.session['user']){
        res.json({
            username: req.session['user'].username
        });
    }else{
        res.status(401).json({msg:"未登入"});
    }
});





//-------------------------------------Google Login-----------------------------------------------
// import crypto from 'crypto';
// usersRoutes.get('/login/google',async (req,res)=>{
//     const accessToken = req.session?.['grant'].response.access_token;
    
//     const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
//         headers:{
//             "Authorization":`Bearer ${accessToken}`
//         }
//     });
//     const userInfo = await fetchRes.json();
//     let users:User[] = (await client.query('SELECT * from users where username = $1',
//             [userInfo.email])).rows;
//     let user = users[0];
//     if(!user){
//         const randomPassword = crypto.randomBytes(20).toString('hex');
//         const bcryptPassword=await hashPassword(randomPassword)
//         const result = await client.query('INSERT INTO users (username,password) values ($1,$2) returning id',
//                         [userInfo.email,bcryptPassword]);
//         console.log(result)
//         user = {
//             username: userInfo.email,
//             password: randomPassword,
//             is_super_user:0,
//         };
//     }
//     req.session['user'] = user;
//     res.status(200).json({success:true});  
// });
//-------------------------------------Google Login-----------------------------------------------





usersRoutes.get('/logout',async(req,res)=>{
    delete req.session['user'];
    res.json({success:true});
});


export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) { 
    if (req.session['user']) {
        next();
    } else {
      
        res.json({msg:"Please log in first."});
    }
}