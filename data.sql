-- RUNRUNRUNRUNRUNRUNRUNRUNRUNRUNRUN
CREATE TABLE subscription_packages(
    id SERIAL                   primary key,
    name                        TEXT,
    description                 VARCHAR(255),
    price                       DECIMAL
);


CREATE TABLE subscription_items(
    id SERIAL                   primary key,
    product_id                  INTEGER,
    FOREIGN KEY (product_id) REFERENCES products(id),
    subscription_batches_id    INTEGER,
    FOREIGN KEY (subscription_batches_id) REFERENCES subscription_batches(id),
    quantity                    INTEGER
);

CREATE TABLE subscription_packages(
    id SERIAL                   primary key,
    name                        TEXT,
    description                 VARCHAR(255),
    price                       DECIMAL
);
select * FROM subscription_packages

INSERT INTO subscription_packages(name, description, price) values
('Regular Size','Regular Size Description', 250),
('Family Size','Family Size Description', 350);


CREATE TABLE signature_items(
    id SERIAL                   primary key,
    name                        VARCHAR(255),
    description                 VARCHAR(255),
    image                       VARCHAR(255)
);
SELECT * FROM signiture_items;
INSERT into signiture_items(name,description,image)values
('signatureVegie!','signatureVegie! Description','image');


CREATE TABLE subscription_batches(
    id SERIAL                   primary key,
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    signiture_items_id          INTEGER,
    FOREIGN KEY (signiture_items_id) REFERENCES signiture_items(id),
    launch_date                 TIMESTAMP,
    off_shelf_date              TIMESTAMP,
    is_active                   INTEGER
);

INSERT into subscription_items(product_id,subscription_packages_id,quantity) values
(1,1,2),(2,1,2),(3,1,2),(4,1,2),(5,1,2),
(1,2,3),(2,2,3),(3,2,3),(4,2,3),(5,2,3);

select * from subscription_batches join subscription_packages on subscription_batches.subscription_packages_id  = subscription_packages.id;
select * from subscription_batches join signiture_items on subscription_batches.signiture_items_id  = signiture_items.id;
INSERT INTO subscription_batches(subscription_packages_id,signiture_items_id,launch_date,off_shelf_date,is_active)values
(1,1,'2021-08-01','2021-08-10',1);
INSERT INTO subscription_batches(subscription_packages_id,signiture_items_id,launch_date,off_shelf_date,is_active)values
(1,1,'2021-08-10','2021-08-20',1);


-- RUNRUNRUNRUNRUNRUNRUNRUNRUNRUNRUN


select * from products join categories on products.category_id = categories.id;
insert into products(name, price, stock, image, description, category_id) values 
    ('vegetable 1', 100,5, 'image', 'vegetable 1 description',1),
    ('vegetable 2', 100,5, 'image', 'vegetable 2 description', 1),
    ('vegetable 3', 100,5, 'image', 'vegetable 3 description', 1),
    ('vegetable 4', 100,5, 'image', 'vegetable 4 description', 1),
    ('herb 1', 200,10, 'image', 'herb 1 description', 2),
    ('herb 2', 200,10, 'image', 'herb 2 description', 2),
    ('herb 3', 200,10, 'image', 'herb 3 description', 2),
    ('herb 4', 200,10, 'image', 'herb 4 description', 2),
    ('edible flower 1',250, 10, 'image', 'edible flower 1 description', 3),
    ('edible flower 2',250, 10, 'image', 'edible flower 2 description', 3),
    ('edible flower 3',250, 10, 'image', 'edible flower 3 description', 3),
    ('edible flower 4',250, 10, 'image', 'edible flower 4 description', 3);

CREATE TABLE subscription_packages(
    id SERIAL                   primary key,
    name                        TEXT,
    description                 VARCHAR(255),
    price                       DECIMAL
);
select * FROM subscription_packages
INSERT INTO subscription_packages(name, description, price) values
('Regular Size','Regular Size Description', 250),
('Family Size','Family Size Description', 350);

CREATE TABLE subscription_items(
    id SERIAL                   primary key,
    product_id                  INTEGER,
    FOREIGN KEY (product_id) REFERENCES products(id),
    subscription_batches_id    INTEGER,
    FOREIGN KEY (subscription_batches_id) REFERENCES subscription_batches(id),
    quantity                    INTEGER
);

alter table subscription_items RENAME COLUMN subscription_packages_id to subscription_batches_id;
SELECT * FROM subscription_items join products on subscription_items.product_id = products.id;
SELECT * FROM subscription_items join subscription_batches on subscription_items.subscription_batches_id = subscription_batches.id;

INSERT into subscription_items(product_id,subscription_packages_id,quantity) values
(1,1,2),(2,1,2),(3,1,2),(4,1,2),(5,1,2),
(1,2,3),(2,2,3),(3,2,3),(4,2,3),(5,2,3);

SELECT * FROM subscription_packages where id = 1;

insert into subscription_items(product_id, subscription_packages_id, quantity) values 

CREATE TABLE signiture_items(
    id SERIAL                   primary key,
    name                        VARCHAR(255),
    description                 VARCHAR(255),
    image                       VARCHAR(255)
);
SELECT * FROM signiture_items;
INSERT into signiture_items(name,description,image)values
('signatureVegie!','signatureVegie! Description','image');



CREATE TABLE products(
    id SERIAL                   primary key,
    name                VARCHAR(255),
    price                       DECIMAL,
    stock                       INTEGER,
    image                       TEXT,
    description                 TEXT,
    category_id                 INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);


select * from products join categories on products.category_id = categories.id;
insert into products(name, price, stock, image, description, category_id) values 
    ('vegetable 1', 100,5, 'image', 'vegetable 1 description',1),
    ('vegetable 2', 100,5, 'image', 'vegetable 2 description', 1),
    ('vegetable 3', 100,5, 'image', 'vegetable 3 description', 1),
    ('vegetable 4', 100,5, 'image', 'vegetable 4 description', 1),
    ('herb 1', 200,10, 'image', 'herb 1 description', 2),
    ('herb 2', 200,10, 'image', 'herb 2 description', 2),
    ('herb 3', 200,10, 'image', 'herb 3 description', 2),
    ('herb 4', 200,10, 'image', 'herb 4 description', 2),
    ('eatable flower 1',250, 10, 'image', 'eatable flower 1 description', 3),
    ('eatable flower 2',250, 10, 'image', 'eatable flower 2 description', 3),
    ('eatable flower 3',250, 10, 'image', 'eatable flower 3 description', 3),
    ('eatable flower 4',250, 10, 'image', 'eatable flower 4 description', 3);




CREATE TABLE categories(
    id SERIAL                   primary key,
    category_name                        TEXT,
    category_description                 TEXT
);
select * from categories;
insert into categories (category_name, category_description) values 
    ('vegetable', 'vegetable description'),
    ('herb', 'herb description'),
    ('edible flower', 'edible flower description');

subscription_packages_id = 1,2

SELECT price from subscription_packages WHERE name = 'Regular Size';
SELECT price from subscription_packages WHERE id = 2;

CREATE TABLE subscription_batches(
    id SERIAL                   primary key,
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    signiture_items_id          INTEGER,
    FOREIGN KEY (signiture_items_id) REFERENCES signiture_items(id),
    launch_date                 TIMESTAMP,
    off_shelf_date              TIMESTAMP,
    is_active                   INTEGER
);
select * from subscription_batches join subscription_packages on subscription_batches.subscription_packages_id  = subscription_packages.id;
select * from subscription_batches join signiture_items on subscription_batches.signiture_items_id  = signiture_items.id;
INSERT INTO subscription_batches(subscription_packages_id,signiture_items_id,launch_date,off_shelf_date,is_active)values
(1,1,'2021-08-01','2021-08-10',1);
INSERT INTO subscription_batches(subscription_packages_id,signiture_items_id,launch_date,off_shelf_date,is_active)values
(1,1,'2021-08-10','2021-08-20',1);



update products set image='vege(1).jpg' where name = 'begonia';

select * from subscription_batches 

SELECT products.*  from  subscription_batches
            INNER JOIN subscription_items on subscription_items.subscription_batches_id = subscription_batches.id
            INNER JOIN products on subscription_items.product_id = products.id
            WHERE NOW() BETWEEN launch_date AND off_shelf_date AND is_active = 1;

------------------------------------
CREATE TABLE subscriptions(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    deliver_id                  INTEGER,
    FOREIGN KEY (deliver_id) REFERENCES deliveries(id),
    start_date                  TIMESTAMP,
    end_date                    TIMESTAMP,
    next_payment_date           TIMESTAMP,
    frequency                   VARCHAR(255),
    charge                      DECIMAL
);

CREATE TABLE subsciptions_recurring_payments(
    id SERIAL                   primary key,
    subscription_id             INTEGER,
    FOREIGN KEY (subscription_id) REFERENCES subscriptions(id),
    period_start_date           TIMESTAMP,
    period_end_date             TIMESTAMP,
    charge_interval             VARCHAR(255),
    billing_address             VARCHAR(255),
    invoice                     VARCHAR(255)
);



