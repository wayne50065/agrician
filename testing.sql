CREATE TABLE users(
    id SERIAL                   primary key,
    username                    VARCHAR(255),
    password                    VARCHAR(255),
    is_super_user               integer
);

CREATE TABLE customers(
    id SERIAL                   primary key,
    users_id                    integer,
    FOREIGN KEY (users_id) REFERENCES users(id),
    address                     TEXT,
    email                       VARCHAR(255),
    date_of_birth               DATE,
    gender                      TEXT,
    mailing_list                INTEGER
);

CREATE TABLE statuses(
    id SERIAL                   primary key,
    detail                      TEXT
);

CREATE TABLE deliveries(
    id SERIAL                   primary key,
    delivery_method             TEXT,
    statuses_id                 INTEGER,
    FOREIGN KEY (statuses_id) REFERENCES statuses(id)
);

CREATE TABLE subscriptions(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    deliver_id                  INTEGER,
    FOREIGN KEY (deliver_id) REFERENCES deliveries(id),
    start_date                  TIMESTAMP,
    end_date                    TIMESTAMP,
    next_payment_date           TIMESTAMP,
    frequency                   VARCHAR(255),
    charge                      DECIMAL
);

CREATE TABLE subsciptions_recurring_payments(
    id SERIAL                   primary key,
    subscription_id             INTEGER,
    FOREIGN KEY (subscription_id) REFERENCES subscriptions(id),
    period_start_date           TIMESTAMP,
    period_end_date             TIMESTAMP,
    charge_interval             VARCHAR(255),
    billing_address             VARCHAR(255),
    invoice                     VARCHAR(255)
);

insert into users (username,password,is_super_user) values
    ('lyn@tecky.io','$2a$10$i7RUDGxDUDtGjSeuHLz87eAQ9WdDh9TLCv8UMQTr9ZC7qriCWUBcy',1);

update products set image='herbs(1).png', name='Perejil Crespo' where name like 'herb 1';
update products set image='herbs(2).png', name='Cilantro' where name like 'herb 2';
update products set image='herbs(3).png', name='Lovage' where name like 'herb 3';
update products set image='herbs(4).png', name='Dill' where name like 'herb 4';
update products set image='vege(1).jpg', name='Rosa Lettuce' where name like 'vegetable 1';
update products set image='vege(2).jpg', name='Rocket Lettuce' where name like 'vegetable 2';
update products set image='vege(3).jpg', name='Broccolini' where name like 'vegetable 3';
update products set image='vege(4).jpg', name='Spanish Frisee Lettuce' where name like 'vegetable 4';
update products set image='edible_flower_1.jpg', name='begonia' where name like 'edible flower 1';
update products set image='edible_flower_2.jpg', name='bachelor buttons' where name like 'edible flower 2';
update products set image='edible_flower_3.jpg', name='borage flowers' where name like 'edible flower 3';
update products set image='edible_flower_4.jpg', name='marigold' where name like 'edible flower 4';

select * from products;

SELECT * from users;

delete from users where id=5;
