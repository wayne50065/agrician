import express, { Request, Response, NextFunction } from 'express';

// import expressSession from 'express-session';





export const adminRoute = express.Router();
adminRoute.use(express.json());
adminRoute.use(express.urlencoded({extended:true}))

export function isAdminAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session['admin']) {
        next();
    } else {
        res.status(401).json({msg:"UnAuthorized"});
    }
}