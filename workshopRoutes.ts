import express from 'express';
import expressSession from 'express-session';
 import {stripe} from './app';





export const workshopRoutes=express.Router();
workshopRoutes.use(express.json());
workshopRoutes.use(express.urlencoded({extended:true}))


workshopRoutes.use(expressSession({
    secret: 'agrician',
    resave: true,
    saveUninitialized: true
}))



// const event= new Map([
//     [1, {priceInCents:10000, name:"AAAAA"}],
//     [2, {priceInCents:20000, name:"BBBBB"}]
// ])

workshopRoutes.get('/create-workshop-checkout',async (req,res)=>{
    let line_items=[]
    line_items=[
        {
            price_data:{
                currency: 'hkd',
                product_data:{
                    name: "Event_1"
                },
                unit_amount:10000
            },
            quantity: 1
        },
        {
            price_data:{
                currency: 'hkd',
                product_data:{
                    name: "Event_1"
                },
                unit_amount:10000
            },
            quantity: 2
        }
    ]
    const session= await stripe.checkout.sessions.create({
        payment_method_types:['card'],
        line_items: line_items,
        mode:'payment',        
        success_url:'http://localhost:8080/success.html',
        cancel_url:'http://localhost:8080/cancel.html'
    })
    
    res.json(session.url)
})