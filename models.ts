export interface User{
    username:string,
    password:string,
    is_super_user:number
}
