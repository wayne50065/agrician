export async function loadCurrentUser() {
  const rand = (new Date()).getTime();
  const res = await fetch(`/current-user?rand=${rand}`);
  const result = await res.json();
    if (result.username) {
        //登入左就轉login畫面內容
        document.querySelector('#login-icon').innerHTML=`<i class="fas fa-user-check" id="logged-in"></i>`;

        // document.querySelector('#userLoggedIn')                
        //     .innerHTML = `<div id="username">User: ${result.username}</div>`;
        document.querySelector('#form-input-content')
            .innerHTML = `<div id="loggedIn-msg">Welcome back!<div id="loggedIn-user">${result.username}</div></div>`;
        document.querySelector('#Login-btn').classList.add('btn-hide')
        document.querySelector('#Logout-btn').classList.remove('btn-hide')
        document.querySelector('#create-user-btn').innerHTML=""
        document.querySelector('#Logout-btn').onclick = async (event) => {
            event.preventDefault();
            const res = await fetch('/logout');
            loadCurrentUser()
            if (await res.status === 200) {
              $('#login-form-Center').modal('hide');
            }
          }
    } else {
        //登出左就變返正常登入畫面
        document.querySelector('#login-icon').innerHTML=`<i class="fas fa-user-alt"></i>`;
        // document.querySelector('#userLoggedIn').innerHTML = "";
        document.querySelector('#Login-btn').classList.remove('btn-hide')
        document.querySelector('#Logout-btn').classList.add('btn-hide')
        document.querySelector('#create-user-btn').innerHTML = `<a href="register.html" >
            <input type="button" class="btn btn-primary" value="Create User"></a>`
        document.querySelector('#form-input-content')
            .innerHTML = `
            <div class="login-form-contentBox">
            <div id="login-title">Login</div>
          </div>
          <div class="login-form-contentBox">
            <input type="text" name="username" placeholder="email">
          </div>
          <div class="login-form-contentBox">
            <input type="password" name="password" placeholder="password">
          </div>
          `
    };



    
    //clear alert msg box when login screen is closed
    $('#login-form-Center').on('hidden.bs.modal', () =>{
        document.querySelector('#login-alert-container').innerHTML=""
      })
}