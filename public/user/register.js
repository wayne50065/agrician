import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from './user/current-user.js';
  footer();
  header();
  
window.onload = () => {
  loadCurrentUser();
  formSubmit();
}

document.querySelector('#register-form').onsubmit = async function(event){
    event.preventDefault();
    const form= event.target;
    const formObj={
        username:form.username.value,
        password:form.password.value,
        password2:form.password2.value
    }
    const res = await fetch('/register',{
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });

    const result = await res.json();
    if(res.status === 200){
        console.log("success");
        document.querySelector('#successArea').innerHTML =
        `<div class="alert alert-success" role="alert">
                ${result.msg}
          </div>`
          document.querySelector('#alert-container')
            .innerHTML = ``;
        window.location = "/";
        
    }else if(res.status === 400 || res.status === 401){
        document.querySelector('#alert-container')
            .innerHTML = `<div class="alert alert-danger" role="alert">
                ${result.msg}
          </div>`;
    }
}

const formSubmit = () => {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const form = document.querySelector('.needs-validation');
    // Loop over them and prevent submission
  
    form.addEventListener('submit',async function (event) {
      event.preventDefault();
      if (form.checkValidity() === false) {
        form.classList.add('was-validated');
        return;
      }
    //   const valid = await fetch('/create-subCheckout-session');
    //   const result = await valid.json();
    // //   window.location = result;
  
    }, false);
  }


// document.querySelector('#register-form')
//       //.onsubmit= async (event)=>{
//         .addEventListener('submit',async (event)=>{        
//          event.preventDefault();

//          const form=event.target;
         
//          const formObj = {
//              username: form.username.value,
//              password: form.password.value,
//              is_super_user:0
//         }

//         const res = await fetch('/register',{
//                     method:"POST",
//                     headers:{
//                         "Content-Type":"application/json"
//                     },
//                     body: JSON.stringify(formObj)
//         });
//         console.log(await res.json())
//         form.reset();
//     })