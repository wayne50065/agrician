import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from './user/current-user.js';

  header();
  footer();
  window.onload=()=>{
  loadSummary();
  loadCurrentUser();
  deliveryMethod()
  formSubmit();
  }


async function loadSummary() {
  const res = await fetch('/summary');
  const chosenPlan = await res.json();

  let chargeInterval;
  if (chosenPlan.frequency == "weekly") {
    chargeInterval = "4 weeks";
  } else if (chosenPlan.frequency == "bi-weekly") {
    chargeInterval = "8 weeks";
  }
  document.querySelector('#summary').innerHTML =
    `<div class="summary">
    Plan: ${chosenPlan.frequency} subscription <br>
    Size: ${chosenPlan.size} <br>
    $HKD ${chosenPlan.price * 4} every ${chargeInterval}.
  </div>`
}



function deliveryMethod(){
  document.querySelector('#delivery-method').addEventListener('click', async(event)=>{
      if(document.querySelector('#exampleRadios1').checked){
          //self pick up
          console.log('self pick up is chosen')
          document.querySelector('#address-form').innerHTML = '';

      } else if (document.querySelector('#exampleRadios2').checked){
          //delivery
          console.log('delivery is chosen')
          document.querySelector('#address-form').innerHTML = 
          `
          <div class="col-md-12 mb-3">
              <label for="validationCustom03">Address</label>
              <input type="text" class="form-control" id="validationCustom03" placeholder="Address"
                  required>
              <div class="invalid-feedback">
                  Please provide a valid address.
              </div>
          </div>
          `
      }
  })
}


// Example starter JavaScript for disabling form submissions if there are invalid fields
const formSubmit = () => {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const form = document.querySelector('.needs-validation');
  // Loop over them and prevent submission

  form.addEventListener('submit',async function (event) {
    event.preventDefault();
    if (form.checkValidity() === false) {
      form.classList.add('was-validated');
      return;
    }
    const valid = await fetch('/create-subCheckout-session');
    const result = await valid.json();
    window.location = result;

  }, false);
}


// function checkOutFrom() {
//   async function checkOut() {
//     document.querySelector('#payment')
//       .addEventListener('submit', async (event) => {
//         event.preventDefault();
//         console.log("clicked on payment");
//         const res = await fetch('/create-subCheckout-session', {
//           method: "GET",
//         })
//         const result = await res.json();
//         console.log(result)
//         window.location = result;
//       })
//   }
// }



// function checkOutFrom(){
// if(document.forms["checkoutForm"].onsubmit()){
// document.querySelector('#payment').addEventListener('click', async (event)=>
// {
//   event.preventDefault();
//   console.log("clicked on payment");
//  const res = await fetch ('/create-subCheckout-session',{
//     method: "GET",
//     })
//     const result = await res.json();
//     console.log(result)
//     window.location = result;
//   })}
// }