import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from '../user/current-user.js';


    footer();
    header();
    window.onload = () => {
        loadCurrentUser();
        openCart();
        loadBatch();
    }

async function loadBatch(){
    // getting data from server
    const res = await fetch('/batch', {
        method: "GET"
    });
    const currentBatch = await res.json();

    const batchArea = document.querySelector('.subscription-product-container');
    batchArea.innerHTML = '';
    for (let i = 0; i < currentBatch.length; i++){
        if(i % 2 == 0){
            batchArea.innerHTML += `
            <div class="subscription-product-item">
            <div class="subscription-product-name">${currentBatch[i].name}</div>
            <div class="subscription-product-img-box"><img class="subscription-product-img" src="/image/${currentBatch[i].image}"></div>
            <div class="subscription-product-description">${currentBatch[i].description}</div>
          </div>`
       
    }else{
        batchArea.innerHTML +=
        `<div class="subscription-product-item">    
            <div class="subscription-product-description">${currentBatch[i].description}</div>
            <div class="subscription-product-img-box"><img class="subscription-product-img" src="/image/${currentBatch[i].image}"></div>
            <div class="subscription-product-name">${currentBatch[i].name}</div>
              </div>`
        
    }}
    }




document.querySelector('#subscription-form').addEventListener('submit',async function(event){
    event.preventDefault();
    const form = event.target;
    
    
    const formObj = {
        frequency: form.frequency.value,
        size: form.size.value,
        chargeInterval: await (document.querySelector('#frequency').selectedOptions[0].getAttribute('charge-interval')
        ),
    }
    const res = await fetch('/subscriptionPage', {
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj),
    })
    const chosenPlan = await res.json();
    const totalPrice = document.querySelector('#price');
    totalPrice.innerHTML = `<p> You have chosen ${chosenPlan.size} package with ${chosenPlan.frequency} plan.<br>
    <br> Total: HKD${chosenPlan.price * 4} every ${chosenPlan.chargeInterval} weeks</p>`

    const checkOutBtn = document.querySelector('#CheckOut');
    checkOutBtn.style = "display:block;"
   console.log(chosenPlan)
})

// function toggleSubmitCheckOut(){
// const submitBtn = document.querySelector('#submit')
// const checkOutBtn = document.querySelector('#CheckOut')

//     if(submitBtn.style == "on") {
//        item.className="off";
//     } else {
//        item.className="on";
//     }
//  }


document.querySelector('#CheckOut').onclick = 
async function(){
const res2 = await fetch(`/current-user`);
const result2 = await res2.json();
if (!result2.username) {
    document.querySelector('#subscription-alert-container').innerHTML='<div class="alert alert-danger">Members only<br>Please login first.</div>'
}else{
    const res = await fetch('/checkPlanSelected');
    const result = await res.json();
    if(res.status === 200){
        window.location = '/subCheckout.html';
    }else{
        document.querySelector('.alert-container')
            .innerHTML = 
            `${result.msg}`;
}

}}

$('#subscription-box').on('hidden.bs.modal', () =>{
    document.querySelector('#subscription-alert-container').innerHTML=""
  })




