import {
  loadCurrentUser
} from './current-user.js';
export function header() {
  document.querySelector('header').innerHTML =
    `<div id="header-bar">
        <div id="header-bar-logo">
          <a href="index.html"><img src="./image/white_no_branding.png" alt=""></a>
        </div>
        <div class="navbar-main">
          <button type="toggle" id="menu-btn" class="">
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-list menu" viewBox="0 0 16 16">
              <path fill-rule="evenodd"
                d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
            </svg>
          </button>
          <button type="toggle" id="iconbar-btn" class="">
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-list menu" viewBox="0 0 16 16">
              <path fill-rule="evenodd"
                d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
            </svg>
          </button>
          
          <div class="navbar-left">
            <a class="navbar-item" href="index.html" id="index">
              <p>HOME</p>
            </a>
            <a class="navbar-item" href="aboutUs.html" id="aboutUs">
              <p>ABOUT US</p>
            </a>
            <a class="navbar-item" href="workshop.html" id="workshop">
              <p>WORKSHOP</p>
            </a>
            <a class="navbar-item" href="store.html" id="store">
              <p>STORE</p>
            </a>
            <a class="navbar-item" href="subscriptionPage.html" id="subscriptionPage">
              <p>SUBSCRIPTION</p>
            </a>
          </div>
          
          <div class="navbar-right">
            <!-- SEARCH icon -->
            <div class="nav-item" id="search-icon">
              <a href="comingsoon.html">
                <i class="fas fa-search"></i>
              </a>          
            </div>
            <!-- LOGIN icon -->
            <div class="nav-item">
              <button type="button" id="login-icon" data-toggle="modal" data-target="#login-form-Center" class="">
                <i class="fas fa-user-alt"></i>
              </button>
            </div>
            <!-- CART icon -->
            <div class="nav-item">
              <button type="button" id="cart-icon" data-toggle="modal" data-target="#exampleModal">
                <i class="fas fa-shopping-cart"></i>
              </button>
            </div>
            <!-- LANGUAGE icon -->
            <div class="nav-item" id="globe-icon">
              <a href="comingsoon.html">
                <i class="fas fa-globe-asia"></i>
              </a>
              
            </div>
          </div>
          
        </div>
      </div>
    `;
  let navbarItems = document.querySelectorAll('.navbar-item');
  let navbarLeft = document.querySelector('.navbar-left');
  let navbarRight = document.querySelector('.navbar-right')
  let navbarMenuBtn = document.querySelector('#menu-btn');
  let navbariconBtn = document.querySelector('#iconbar-btn');
  let cartBtn = document.querySelector('#cart-icon');
  let loginBtn = document.querySelector('#login-icon');
  let currentLocation = window.location.pathname.split('.')[0].slice(1)

  cartBtn.onclick = () => {
    navbarRight.classList.remove('active');
  }

  loginBtn.onclick = () => {
    navbarRight.classList.remove('active');
  }

  $(window).resize(() => {
    if ($('#menu-btn').css('display') == 'none' && navbarLeft.classList.contains('active')) {
      navbarLeft.classList.remove('active');
    }
    if ($('#iconbar-btn').css('display') == 'none' && navbarRight.classList.contains('active')) {
      navbarRight.classList.remove('active');
    }
  })




  for (let i = 0; i < navbarItems.length; i++) {
    navbarItems[i].onclick = () => {
      let j = 0;
      while (j < navbarItems.length) {
        navbarItems[j++].className = 'navbar-item';
      };
      navbarItems[i].className = 'navbar-item active';
    }
  }

  navbarMenuBtn.onclick = () => {
    navbarLeft.classList.toggle('active');
    navbarRight.classList.remove('active');
  }
  navbariconBtn.onclick = () => {
    navbarLeft.classList.remove('active');
    navbarRight.classList.toggle('active');
  }

  function loadCurrentActivePage() {

    let changeActive = document.querySelector(`#${currentLocation}`);
    if (changeActive != null) {
      changeActive.className = 'navbar-item active';
    }

  }
  loadCurrentActivePage()
}
export function footer() {
  document.querySelector('footer').innerHTML = `<div class="footer">
    <div class="row">
    <div class="col-md-6 footer-outer-container">
      <div class="footer-container">
        <a href='/aboutUs.html' class="footer-header">
          ABOUT US
        </a>
        <div class="footer-item">
          <a href='comingsoon.html' class='footer-link'>our mission</a>
          <a href='comingsoon.html' class='footer-link'>urban farming</a>
        </div>
      </div>
  
      <div class="footer-container">
        <a href='/workshop.html' class="footer-header">WORKSHOP</a>
        <div class="footer-item">
          <a href='comingsoon.html' class='footer-link'>gallery</a>
          <a href='comingsoon.html' class='footer-link'>availability</a>
        </div>
      </div>
      </div>
  
      <div class="col-md-6 footer-outer-container">
      <div class="footer-container">
        <a  href='/store.html' class="footer-header">STORE</a>
        <div class="footer-item">
          <a href='comingsoon.html' class='footer-link'>plant</a>
          <a href='comingsoon.html' class='footer-link'>flower</a>
          <a href='subscriptionPage.html' class='footer-link'>Subscription</a>
        </div>
      </div>
  
      <div class="footer-container">
        <a href='comingsoon.html' class="footer-header">ENQUIRY</a>
        <div class="footer-item">
          <a href='comingsoon.html' class='footer-link'>contact us</a>
          <a href='https://www.instagram.com/agrician/?hl=en' class='footer-link'>follow us on Instagram</a>
          <a href='comingsoon.html' class='footer-link'>FAQ</a>
        </div>
      </div>
      </div>
    </div>
  </div>`;

}
//render product to cart pop up
export async function renderCart() {
  const res = await fetch('/checkCart', {
    method: 'GET'
  })
  let cartPopUp = document.querySelector('#inside-cart');
  const data = await res.json()
  if (data.msg == 'empty cart') {
    cartPopUp.innerHTML = `<span id="empty-cart">nothing in cart now...<br>go pick something for yourself ^_^ </span>`;
  } else {

    cartPopUp.innerHTML = "";
    console.log(data)
    for (let product in data) {
      cartPopUp.innerHTML +=
        `
        <div class="cart-item row">
            <div class="cart-img col-sm-6">
              <div class="img-fluid" product_name="${product}">
              <i class="fas fa-times-circle delete-btn"></i>
               <img class="img-inside-cart" src="/image/${data[product]['img']}">
              </div>
            </div>
            <div class="cart-product-info col-sm-6">
                <div class="cart-product-name">
                    ${product}
                </div>
                <div class="cart-product-description">
                    ${data[product]['description']}
                </div>
                <div class="cart-product-qty">
                    quantity: ${data[product]['qty']}
                </div>
                <div class="cart-price-container">
                    <div class="cart-product-price-header">unit price</div>
                    <div class="cart-product-price">${data[product]['price']}</div>
                </div>
            </div>
        </div>  
        `
    }
  }
  removeFromCart()
}
// remove item from cart
export async function removeFromCart() {
  const delBtns = document.querySelectorAll('.delete-btn');
  for (let btn of delBtns) {
    btn.addEventListener('click', async (event) => {
      event.preventDefault()
      const target = btn.parentElement.getAttribute('product_name');
      console.log(target)
      const res = await fetch(`remove/${target}`, {
        method: "DELETE"
      })
      const result = await res.json()
      if (result.msg = 'remove succes') {
        renderCart()
      }

    })
  }
}
export function openCart() {
  document.querySelector('#cart-icon').addEventListener('click', async (event) => {
    await renderCart()
    removeFromCart();
  })
}

document.querySelector('div.login-modal').innerHTML = `
<div class="modal fade" id="login-form-Center" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content glass-bg">
        <form action="/login" method="post" id="login-form">
          <div class="login-modal-header">
            <div class="login-modal-icon">
              <img src="./image/white_no_branding.png" alt="">
            </div>
              Agrician
            <div class="login-modal-icon"></div>
          </div>
          <div class="modal-body form-body">
            <div id="form-input-content">
              <div class="login-form-contentBox">
                <div id="login-title">Login</div>
                <hr>
              </div>
              
              <div class="login-form-contentBox">
                <input type="text" name="username" placeholder="  Email">
              </div>
              <div class="login-form-contentBox">
                <input type="password" name="password" placeholder="  Password">
              </div>
            </div>
          </div>
  
          <div class="login-modal-footer">
            <div id="create-user-btn">
              <a href="register.html">
                <button type="button" class="btn btn-primary login-screen-btn" value="Create User">Create User</button>
              </a>
            </div>  
            <div class="user-login-btn">
              <button type="button" class="btn btn-secondary" id="login-close-btn" data-dismiss="modal">Close</button>
              <div id="login-logout-btn">
                <button type="submit" class="btn btn-primary login-screen-btn" id="Login-btn" value="Login">Login</button>
                <a href="/logout" class="btn btn-primary login-screen-btn btn-hide" id="Logout-btn">Logout</a>
              </div>
            </div>
          </div>
        </form>
        <div id="login-alert-container"></div>
      </div>
    </div>
  </div>`;

//cart
document.querySelector('.cart-modal').innerHTML = `
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog cart-modal" role="document">
                                    <div class="modal-content glass-bg" id="cart">
                                        <div class="modal-body" id="inside-cart">
                                            sorry nothing in cart yet...
                                        </div>
                                        <div class="modal-footer" id="cart-footer">
                                            <div><a type="button" class="btn btn-success"
                                                id="checkout-btn">CHECK OUT</a></div>
                                        </div>
                                        <div id="cart-alert-container"></div>
                                    </div>
                                </div>
  </div>`;

$('#exampleModal').on('hidden.bs.modal', (event) => {
  document.querySelector('#cart-alert-container').innerHTML = ""
})


document.querySelector("#checkout-btn").onclick = async (event) => {
  event.preventDefault()
  const res2 = await fetch(`/current-user`);
  const result2 = await res2.json();
  if (!result2.username) {
    document.querySelector('#cart-alert-container').innerHTML = `<div class="alert alert-danger" role="alert">
        Please login first.
        </div>`;
  }
  const res = await fetch('/continue-to-checkout', {
    method: "GET"
  })

  const result = await res.json()
  if (result.msg == 'empty cart') {
    document.querySelector('#cart-alert-container').innerHTML = '<div class="alert alert-danger">Oops...your cart is empty,<br>nothing to check out.</div>'
  } else if (result.msg == 'success') {
    window.location = './checkout.html'
  }
}



document.querySelector('#login-form')
  .addEventListener('submit', async (event) => {
    event.preventDefault();
    const form = event.target;
    const formObj = {
      username: form.username.value,
      password: form.password.value,
    }
    const res = await fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formObj)
    });
    if (res.status === 200) {
      console.log("login success")
      $('#login-form-Center').modal('hide');
    } else {
      console.log("login fail")
      document.querySelector('#login-alert-container')
        .innerHTML = `<div class="alert alert-danger" role="alert">
            Username or Password is not correct!
            </div>`;
    }
    loadCurrentUser();

    form.reset();
  });