import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from './user/current-user.js';


window.onload=()=>{
    loadStorePage();
    openCart();
    loadCurrentUser();
}

    footer();
    header();
    




//render the category and products of the page from database
async function loadStorePage() {

    // getting data from server
    const res = await fetch('/onload', {
        method: "GET"
    });
    const categories = await res.json();
    // render row according to the number of categories
    document.querySelector('.store-content').innerHTML = "";
    for (let category of categories) {
        document.querySelector('.store-content').innerHTML +=
            `
        <div class="store-row row">
        </div>
        `
    }

    // render categories and products on the page
    const rows = document.querySelectorAll('.store-row');
    for (let i = 0; i < rows.length; i++) {
        let productItems = "";
        for (let product of categories[i].products) {
            let qty = 1
            productItems +=
                `
                <!-- Button trigger modal -->
                <button type="button" class="btn product-modal-activation" data-toggle="modal" data-target="#${product.name}">
                    <div class="product-item" product_name="${product.name}">
                        <img class="product-img" src="/image/${product.image}">
                        <div class="product-name">${product.name}</div>
                    </div>
                </button>
    
                <!-- Modal -->
                <div class="modal fade product-modal-pop-up" id="${product.name}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content glass-bg product-modal-container">
                    <div class="modal-header product-modal-header">
                        <h5 class="modal-title product-modal-title" id="${product.name}" name="${product.name}">${product.name}</h5>
                    </div>
                    <div class="modal-body">
                        <form class="product-box row">
                            <div class="col-sm-6 product-detail">
                                <img class="store-modal-img" src="/image/${product.image}">
                                <div class="qty-control">
                                    <i class="fas fa-minus-circle qty-control-btn minus" product_id_minus="${product.product_id}"></i>
                                    <div name="qty" class="qty" product_id_qty="${product.product_id}">${qty}</div>
                                    <i class="fas fa-plus-circle qty-control-btn plus" product_id_plus="${product.product_id}"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 product-detail">
                                <div class="product-box-description">${product.description}</div>
                                <div class="product-box-price">HKD ${product.price}</div>
                            </div>
                            <div class="modal-footer product-modal-footer">
                                <button type="button" class="btn product-modal-add-to-cart-btn" data-dismiss="modal" product_name="${product.name}" product_id_add = "${product.product_id}"">add to cart</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            `


        }


        // different arrangement for odd and even number rows
        if (i % 2 == 0) {
            rows[i].innerHTML =
                `
                <div class="col-sm-4">
                <div class="category-container">
                    <div class='category-header'>${categories[i].category_name}</div>
                    <div class='category-description'>${categories[i].category_description}</div>
                </div>
            </div>
    
            <div class="col-sm-8">
                <div class="product-container">
                    ${productItems}
                </div>
            </div>
            `
        } else {
            rows[i].classList.add('odd-number-row')
            rows[i].innerHTML =
                `
            <div class="col-sm-8">
                <div class="product-container">
                    ${productItems}
                </div>
            </div>    
            
            <div class="col-sm-4">
                <div class="category-container">
                    <div class='category-header'>${categories[i].category_name}</div>
                    <div class='category-description'>${categories[i].category_description}</div>
                </div>
            </div>
    
            
            `
        }
    }
    addtoCart()
    storeDragToScroll()
    increase()
    decrease()
}

async function increase(){
    const btns = document.querySelectorAll('.plus');
    for (let btn of btns){
        btn.addEventListener('click', (event)=>{
            let productId = event.target.getAttribute('product_id_plus');
            let qty = parseInt(document.querySelector(`[product_id_qty = "${productId}"]`).innerText);
            qty += 1
            document.querySelector(`[product_id_qty = "${productId}"]`).innerText = qty
        })
    }
   
}

async function decrease(){
    const btns = document.querySelectorAll('.minus');
    for (let btn of btns){
        btn.addEventListener('click', (event)=>{
            let productId = event.target.getAttribute('product_id_minus');
            let qty = parseInt(document.querySelector(`[product_id_qty = "${productId}"]`).innerText);
            if (qty !== 0){
                qty -= 1
            } else {
                return
            }
            document.querySelector(`[product_id_qty = "${productId}"]`).innerText = qty
        })
    }
}

async function addtoCart() {

    //add to cart
    const btns = document.querySelectorAll('.product-modal-add-to-cart-btn')
    for (let btn of btns) {
        btn.addEventListener('click', async (event) => {
            event.preventDefault();
            const product = event.target.getAttribute('product_name')
            let productId = event.target.getAttribute('product_id_add');
            let qty = parseInt(document.querySelector(`[product_id_qty = "${productId}"]`).innerText)
            if (qty !== 0) {
                const formObj = {};
                formObj[product] = qty
                const res = await fetch('/addToCart', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(formObj)
                })
                const result = await res.json();
            }
            document.querySelector(`[product_id_qty = "${productId}"]`).innerText = 1
        })
    }
}


async function storeDragToScroll(){
    const containers = document.querySelectorAll('.product-container')
    for(let slider of containers){
        // const slider = document.querySelector('#photo-slide');
        let isDown = false;
        let startX;
        let scrollLeft;
        
        slider.addEventListener('mousedown', (e) => {
          isDown = true;
          slider.classList.add('active');
          startX = e.pageX - slider.offsetLeft;
          scrollLeft = slider.scrollLeft;
        });
        slider.addEventListener('mouseleave', () => {
          isDown = false;
          slider.classList.remove('active');
        });
        slider.addEventListener('mouseup', () => {
          isDown = false;
          slider.classList.remove('active');
        });
        slider.addEventListener('mousemove', (e) => {
          if(!isDown) return;
          e.preventDefault();
          const x = e.pageX - slider.offsetLeft;
          const walk = (x - startX) * 3; //scroll-fast
          slider.scrollLeft = scrollLeft - walk;
        });
    }

}
