import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from './user/current-user.js';
    header()
    footer()

    window.onload=()=>{
    renderCart()
    loadCurrentUser()
    deliveryMethod()
    formSubmit()
    }
    

// render product preview on checkout page
async function renderCart(){
    const res = await fetch('/checkCart', {
        method: "GET"
    });

    const cartItems = await res.json()
    let products = Object.keys(cartItems)
    let cartInfo = document.querySelector('.cart-info')
    cartInfo.innerHTML = ""
    let totalPrice = 0;
    for (let product of products){
        cartInfo.innerHTML +=
        `
        <div class="cart-item">
            <div class="row">
                <div class="col-sm-6">
                    <img class="store-checkout-img" src="/image/${cartItems[product]['img']}">
                </div>
                <div class="col-sm-6">
                    <div class="product-name">${product}</div>
                    <div class="product-qty">${cartItems[product]['qty']} pieces</div>
                    <div class="product-price">HKD ${cartItems[product]['price']}</div>
                </div>
            </div>
        </div>
        `
        totalPrice += parseInt(cartItems[product]['qty']) * parseInt(cartItems[product]['price'])
    }
    cartInfo.innerHTML += 
    `
    <div id="total-price"> Total: HKD ${totalPrice}</div>
    `
}

function deliveryMethod(){
    document.querySelector('#delivery-method').addEventListener('click', async(event)=>{
        if(document.querySelector('#exampleRadios1').checked){
            //self pick up
            document.querySelector('#address-form').innerHTML = '';

        } else if (document.querySelector('#exampleRadios2').checked){
            //delivery
            document.querySelector('#address-form').innerHTML = 
            `
            <div class="col-md-12 mb-3">
                <label for="validationCustom03">Address</label>
                <input type="text" class="form-control" id="validationCustom03" placeholder="Address"
                    required>
                <div class="invalid-feedback">
                    Please provide a valid address.
                </div>
            </div>
            `
        }
    })
}


const formSubmit = () => {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const form = document.querySelector('.needs-validation');
    // Loop over them and prevent submission
  
    form.addEventListener('submit',async function (event) {
      event.preventDefault();
      if (form.checkValidity() === false) {
        form.classList.add('was-validated');
        return;
      }
      const res = await fetch('/create-store-checkout-session', {
        method:"GET"
    })
    
    const result = await res.json();
    if (result.msg == 'empty cart'){
        return
    } else {
        window.location = result;
    }
  
    }, false);
  }