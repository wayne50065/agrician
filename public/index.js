import {header, footer, openCart} from './onloaditems.js';
import { loadCurrentUser } from './user/current-user.js';

  header();
  footer();
  window.onload = () => {
    loadCurrentUser();
    openCart();
    homepageDragToScroll();
  }

  

async function homepageDragToScroll(){
  const homepageSlider = document.querySelector('#photo-slide');
  let isDown = false;
  let startX;
  let scrollLeft;
  
  homepageSlider.addEventListener('mousedown', (e) => {
    isDown = true;
    homepageSlider.classList.add('active');
    startX = e.pageX - homepageSlider.offsetLeft;
    scrollLeft = homepageSlider.scrollLeft;
  });
  homepageSlider.addEventListener('mouseleave', () => {
    isDown = false;
    homepageSlider.classList.remove('active');
  });
  homepageSlider.addEventListener('mouseup', () => {
    isDown = false;
    homepageSlider.classList.remove('active');
  });
  homepageSlider.addEventListener('mousemove', (e) => {
    if(!isDown) return;
    e.preventDefault();
    const x = e.pageX - homepageSlider.offsetLeft;
    const walk = (x - startX) * 3; //scroll-fast
    homepageSlider.scrollLeft = scrollLeft - walk;
  });
}
