select * from users;
DELETE from users where id between 2 and 100;

select * from products;
select * from products join categories on products.category_id = categories.id;
alter table products add column name TEXT;
insert into products(price, stock, image, description, category_id, name) values 
    (100, 5, 'image', 'vegetable 1 description', 1, 'vegetable 1'),
    (100, 5, 'image', 'vegetable 2 description', 1, 'vegetable 2'),
    (100, 5, 'image', 'vegetable 3 description', 1, 'vegetable 3'),
    (100, 5, 'image', 'vegetable 4 description', 1, 'vegetable 4'),
    (200, 10, 'image', 'herb 1 description', 2, 'herb 1'),
    (200, 10, 'image', 'herb 2 description', 2, 'herb 2'),
    (200, 10, 'image', 'herb 3 description', 2, 'herb 3'),
    (200, 10, 'image', 'herb 4 description', 2, 'herb 4'),
    (250, 10, 'image', 'eatable flower 1 description', 3, 'eatable flower 1'),
    (250, 10, 'image', 'eatable flower 2 description', 3, 'eatable flower 2'),
    (250, 10, 'image', 'eatable flower 3 description', 3, 'eatable flower 3'),
    (250, 10, 'image', 'eatable flower 4 description', 3, 'eatable flower 4');

update products set image='edible_flower_1.jpg', name='begonia' where name like 'edible flower 1';
update products set image='edible_flower_2.jpg', name='bachelor buttons' where name like 'edible flower 2';
update products set image='edible_flower_3.jpg', name='borage flowers' where name like 'edible flower 3';
update products set image='edible_flower_4.jpg', name='marigold' where name like 'edible flower 4';

select * from products;




insert into categories (category_name, category_description) values 
    ('vegetable', 'vegetable description'),
    ('herb', 'herb description'),
    ('eatable flower', 'eatable flower description');
select * from categories;
delete from categories where name='vegetable';
update categories set category_name='vegetable' where category_name= 'vagetable';
alter table categories rename column description to category_description;




