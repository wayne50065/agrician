import express from 'express';
import { client,stripe } from './app';
import expressSession from 'express-session';
import { isLoggedInAPI } from './usersRoutes';






export const storeRoutes = express.Router();
storeRoutes.use(express.json());
storeRoutes.use(express.urlencoded({extended:true}))



storeRoutes.use(expressSession({
    secret: 'agrician',
    resave: true,
    saveUninitialized: true,
}))

// send processed product and category data to client side
storeRoutes.get('/onload', async(req, res, next)=>{
    const products = await (await client.query(`select price, stock, products.image, products.name, products.description, products.id as product_id, categories.name as category_name, categories.description as category_description, categories.id as category_id from products join categories on products.category_id=categories.id
    `)).rows;
    
    const categoryMapping = {};
    for (let product of products){
        const {category_id, category_description, category_name, ...others} = product;
        if (category_id in categoryMapping){
            categoryMapping[category_id].products.push(others)
        } else {
            categoryMapping[category_id] = {
                category_id,
                category_name,
                category_description,
                products: [others]
            }
        }
    }
    const result= Object.values(categoryMapping);
    res.json(result)
    // next()
})


// add product to cart when btn is clicked
storeRoutes.post('/addToCart', async(req,res,next)=>{
    const content:string = req.body;
    if(!req.session['cart']){
        // empty cart logic
        req.session['cart'] = content;
        // res.json(req.session['cart']);
    } else {
        // loaded cart logic
        if (Object.keys(content)[0] in req.session['cart']){
            let addingQty = parseInt(Object.values(content)[0]);
            let cartedQty = parseInt(req.session['cart'][Object.keys(content)[0]]);
            let qty = addingQty + cartedQty;
            req.session['cart'][Object.keys(content)[0]] = qty;
            res.json(req.session['cart']);
            return 
        } else {
            req.session['cart'][Object.keys(content)[0]] = Object.values(content)[0];
            res.json(req.session['cart']);
            return 

        }
    }
    next();

})

// middleware before checkout
storeRoutes.get('/continue-to-checkout',isLoggedInAPI, async(req,res, next)=>{
    if(!req.session['cart'] || Object.keys(req.session['cart']).length == 0){
        res.json({msg:'empty cart'})
    } else {
        res.json({msg:'success'})
    }
})

storeRoutes.get('/create-store-checkout-session', async (req,res)=>{
    // getting the items inside cart
    const cart = await req.session['cart'];

    // getting the product name of all the items inside cart into an array
    const cartItems = Object.keys(cart)

    // getting only the product info of the products in cart
    let productInfo = []
    for (let item of cartItems){
        let info  = (await client.query(`select * from products where name = $1`, [item])).rows[0]
        productInfo.push(info)
    }

    //pushing the carted items into an array in the desired form of stripe
    let lineItems = [];
    for (let product of productInfo){
            let productDetails = {
                price_data:{
                    currency: 'hkd',
                    product_data:{
                        name: product['name']
                    },
                    unit_amount: parseInt(product.price)*100
                },
                quantity: parseInt(cart[product['name']])
            }
            lineItems.push(productDetails)
        }   

    //creating checkout session and send the payment url to client side
    const paymentSession = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: lineItems,
        mode: 'payment',
        success_url: 'http://localhost:8080/success.html',
        cancel_url: 'http://localhost:8080/cancel.html'
    })
    req.session['cart'] = {}
    res.json(paymentSession.url)
    return
})
