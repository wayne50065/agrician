import express from 'express';
import { usersRoutes } from './usersRoutes';
import { storeRoutes } from './storeRoute';
import { subRoutes } from './subRoute';
import { adminRoute, isAdminAPI } from './adminRoute'
import { workshopRoutes } from './workshopRoutes';
import {Client} from 'pg';
import dotenv from 'dotenv';
import expressSession from 'express-session';


dotenv.config();

const app = express();



app.use(express.json());
app.use(express.urlencoded({extended:true}));



export const stripe = require('stripe')(process.env.STRIPE_PRIVATE_KEY);

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});


(async ()=>{
    await client.connect();
})();






app.use(expressSession({
    secret: 'agrician',
    resave: true,
    saveUninitialized: true
}))

// app.use((req, res, next) => {
//     console.log(`[info] method: ${req.method}, path: ${req.path}`);
//     next()
// })

app.get('/checkCart', async (req,res,next)=>{
    if(req.session['cart']){
        const productInfo = (await client.query(`select * from products`)).rows;
        const cart = await req.session['cart'];
        let data = {};
        const cartItems = Object.keys(cart);
        for (let item of cartItems){
            if (item in data){
                return
            } else {
                for (let product of productInfo){
                    if (product.name == item){
                        data[item] = {
                            id: product.id,
                            qty: cart[item],
                            description: product.description,
                            img: product.image,
                            price: product.price
                        }
                    }
                }
            }
        }
        if(Object.keys(data).length !== 0){
            res.json(data)
        } else {
            res.json({msg:'empty cart'})
        }
    } else {
        res.json({msg: 'empty cart'})
    }
    
})

app.delete('/remove/:target',/*isAdminAPI,*/ async(req, res, next)=>{
    const target = req.params.target
    delete req.session['cart'][target];
    res.json({msg:'remove success'})
})



app.use('/', workshopRoutes)
app.use('/', usersRoutes);
app.use('/', storeRoutes);
app.use('/', subRoutes);
app.use('/', adminRoute);
app.use('*/favicon.ico',express.static('public/image/favicon.ico'));
app.use(express.static('public'));
app.use(express.static('public/subscription'));
app.use(express.static('public/store'));
app.use(express.static('public/workshop'));
app.use(express.static('public/user'));
app.use(isAdminAPI,express.static('protected'));


app.listen(8080);

// send processed product and category data to client side
// storeRoutes.get('/onload', async(req, res, next)=>{
//     const products = await (await client.query(`select * from products join categories on products.category_id=categories.id`)).rows;
    
//     const categoryMapping = {};
//     for (let product of products){
//         const {category_id, category_description, category_name, ...others} = product;

//         if (category_id in categoryMapping){
//             categoryMapping[category_id].products.push(others)
//         } else {
//             categoryMapping[category_id] = {
//                 category_id,
//                 category_name,
//                 category_description,
//                 products: [others]
//             }
//         }
//     }
//     const result = Object.values(categoryMapping);
//     res.json(result)
// })
