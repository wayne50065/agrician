import express from 'express';
import { client, stripe } from './app';
import expressSession from 'express-session';
import { isLoggedInAPI } from './usersRoutes';


export const subRoutes = express.Router();


subRoutes.use(express.json());
subRoutes.use(express.urlencoded({ extended: true }))


subRoutes.use(expressSession({
    secret: 'agrician',
    resave: true,
    saveUninitialized: true
}))

// subRoutes.get('/',async(req,res,next)=>{
//     req.session['subscription'] = null;
//     next();
// })

subRoutes.post('/subscriptionPage', async (req, res, next) => {
    // const frequency:string = (req.body.frequency);
    // const size:string = (req.body.size);
    const size = req.body.size;
    const frequency = req.body.frequency;
    const chargeInterval= req.body.chargeInterval;
    const price = (await client.query(`SELECT price from subscription_packages WHERE name = $1`, [size])).rows
    const chosenPackage = {
        frequency: frequency,
        size: size,        //size = subscription package id
        price: price[0].price,
        chargeInterval: chargeInterval,
    }
    req.session['subscription'] = chosenPackage;
    console.log(chosenPackage);
    res.json(req.session['subscription']);
    next();
})

subRoutes.get('/summary', async (req, res, next) => {
    const summary = await req.session['subscription'];
    res.send(summary);
    next();
})

subRoutes.get('/batch', async (req, res, next) => {
    const currentBatch = await client.query(`SELECT products.* from  subscription_batches
INNER JOIN subscription_items on subscription_items.subscription_batches_id = subscription_batches.id
INNER JOIN products on subscription_items.product_id = products.id
WHERE NOW() BETWEEN launch_date AND off_shelf_date AND is_active = 1;
`);
    //get product items from current batches 
    let result = currentBatch.rows
    res.json(result);
})



subRoutes.get('/checkPlanSelected',isLoggedInAPI, async (req, res, next) => {
    if (req.session['subscription']) {
        res.status(200).json(req.session['subscription']);
        // URL vs PATH
    } else {
        req.session['subscription'] = null;
        res.status(401).json({ success: false, msg: "No plan is selected!" });
    }
})

subRoutes.get("/create-subCheckout-session", isLoggedInAPI, async (req, res) => {
    const chosenPackage = await (req.session['subscription']);
    console.log(chosenPackage)
    let packageInfo = {
        price_data:{
        currency: 'hkd',
        recurring: {
            interval: 'week',
            interval_count: chosenPackage.chargeInterval,
          },
        product_data: {
          name: chosenPackage.size
        },
        unit_amount: chosenPackage.price * 100,
    },
    quantity: 4,
}
console.log(packageInfo);
    const subSession = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        mode: 'subscription',
        line_items: [packageInfo],
        success_url: `http://localhost:8080/success.html`,
        cancel_url: `http://localhost:8080/cancel.html`
    })
    console.log(subSession.url);
    res.json(subSession.url)
})

