import {headerNfooter} from './public/index.js';
import {loadCurrentUser} from './public/current-user.js';

window.onload = ()=>{
    headerNfooter();
    loadCurrentUser();
}

