import {Client} from 'pg';
import dotenv from 'dotenv';

dotenv.config();

const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

client.connect()


// create table
client.query(`
CREATE TABLE users(
    id SERIAL                   primary key,
    username                    VARCHAR(255),
    password                    VARCHAR(255),
    is_super_user               integer
);

CREATE TABLE customers(
    id SERIAL                   primary key,
    users_id                    integer,
    FOREIGN KEY (users_id) REFERENCES users(id),
    address                     TEXT,
    email                       VARCHAR(255),
    date_of_birth               DATE,
    gender                      TEXT,
    mailing_list                INTEGER
);

CREATE TABLE statuses(
    id SERIAL                   primary key,
    detail                      TEXT
);

CREATE TABLE deliveries(
    id SERIAL                   primary key,
    delivery_method             TEXT,
    statuses_id                 INTEGER,
    FOREIGN KEY (statuses_id) REFERENCES statuses(id)
);

CREATE TABLE subscription_packages(
    id SERIAL                   primary key,
    name                        TEXT,
    description                 VARCHAR(255),
    price                       DECIMAL
);

CREATE TABLE subscriptions(
    id SERIAL                   primary key,
    customer_id                 INTEGER,
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    deliver_id                  INTEGER,
    FOREIGN KEY (deliver_id) REFERENCES deliveries(id),
    start_date                  TIMESTAMP,
    end_date                    TIMESTAMP,
    next_payment_date           TIMESTAMP,
    frequency                   VARCHAR(255),
    charge                      DECIMAL
);

CREATE TABLE subscriptions_recurring_payments(
    id SERIAL                   primary key,
    subscription_id             INTEGER,
    FOREIGN KEY (subscription_id) REFERENCES subscriptions(id),
    period_start_date           TIMESTAMP,
    period_end_date             TIMESTAMP,
    charge_interval             VARCHAR(255),
    billing_address             VARCHAR(255),
    invoice                     VARCHAR(255)
);

CREATE TABLE categories(
    id SERIAL       primary key,
    name            TEXT,
    description     TEXT
);

CREATE TABLE products(
    id SERIAL       primary key,
    price           DECIMAL,
    stock           INTEGER,
    image           TEXT,
    description     TEXT,
    category_id     INTEGER,
    name            TEXT,

    FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE carts(
    id SERIAL       primary key,
    product_id      INTEGER,
    customer_id     INTEGER,

    FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE order_items(
    id SERIAL       primary key,
    customer_id     INTEGER,
    product_name    TEXT,
    price           DECIMAL,
    quantity        INTEGER
);

CREATE TABLE orders(
    id SERIAL       primary key,
    customer_id     INTEGER,
    date            TIMESTAMP,
    total_price     DECIMAL,
    delivery_id     INTEGER
);

CREATE TABLE payments(
    id SERIAL       primary key,
    customer_id     INTEGER,
    date            TIMESTAMP,
    amount          DECIMAL,
    method          TEXT,
    billing_address TEXT,
    invoice         TEXT,
    order_id        INTEGER,

    FOREIGN KEY (order_id) REFERENCES orders(id)
);

CREATE TABLE signature_items(
    id SERIAL                   primary key,
    name                        VARCHAR(255),
    description                 VARCHAR(255),
    image                       VARCHAR(255)
);

CREATE TABLE subscription_batches(
    id SERIAL                   primary key,
    subscription_packages_id   INTEGER,
    FOREIGN KEY (subscription_packages_id) REFERENCES subscription_packages(id),
    signature_items_id          INTEGER,
    FOREIGN KEY (signature_items_id) REFERENCES signature_items(id),
    launch_date                 TIMESTAMP,
    off_shelf_date              TIMESTAMP,
    is_active                   INTEGER
);

CREATE TABLE subscription_items(
    id SERIAL                   primary key,
    product_id                  INTEGER,
    FOREIGN KEY (product_id) REFERENCES products(id),
    subscription_batches_id    INTEGER,
    FOREIGN KEY (subscription_batches_id) REFERENCES subscription_batches(id),
    quantity                    INTEGER
);


`)

//insert data
client.query(`
    INSERT INTO subscription_packages(name, description, price) values
    ('Regular Size','Regular Size Description', 250),
    ('Family Size','Family Size Description', 350);

    INSERT into signature_items(name,description,image)values
    ('signatureVegie!','signatureVegie! Description','image');

    insert into categories (name, description) values 
    ('vegetable', 'vegetable description'),
    ('herb', 'herb description'),
    ('edible flower', 'edible flower description');

    insert into products(name, price, stock, image, description, category_id) values 
    ('vegetable 1', 100,5, 'image', 'vegetable 1 description',1),
    ('vegetable 2', 100,5, 'image', 'vegetable 2 description', 1),
    ('vegetable 3', 100,5, 'image', 'vegetable 3 description', 1),
    ('vegetable 4', 100,5, 'image', 'vegetable 4 description', 1),
    ('herb 1', 200,10, 'image', 'herb 1 description', 2),
    ('herb 2', 200,10, 'image', 'herb 2 description', 2),
    ('herb 3', 200,10, 'image', 'herb 3 description', 2),
    ('herb 4', 200,10, 'image', 'herb 4 description', 2),
    ('edible flower 1',250, 10, 'image', 'edible flower 1 description', 3),
    ('edible flower 2',250, 10, 'image', 'edible flower 2 description', 3),
    ('edible flower 3',250, 10, 'image', 'edible flower 3 description', 3),
    ('edible flower 4',250, 10, 'image', 'edible flower 4 description', 3);

    INSERT INTO subscription_batches(subscription_packages_id,signature_items_id,launch_date,off_shelf_date,is_active)values
    (1,1,'2021-08-01','2021-08-10',1);
    INSERT INTO subscription_batches(subscription_packages_id,signature_items_id,launch_date,off_shelf_date,is_active)values
    (1,1,'2021-08-10','2021-08-20',1);

    INSERT into subscription_items(product_id,subscription_batches_id,quantity) values
    (1,1,2),(2,1,2),(3,1,2),(4,1,2),(5,1,2),
    (1,2,3),(2,2,3),(3,2,3),(4,2,3),(5,2,3);

    insert into users (username,password,is_super_user) values
    ('lyn@tecky.io','$2a$10$i7RUDGxDUDtGjSeuHLz87eAQ9WdDh9TLCv8UMQTr9ZC7qriCWUBcy',1);

    update products set image='herbs(1).png', name='Perejil Crespo' where name like 'herb 1';
    update products set image='herbs(2).png', name='Cilantro' where name like 'herb 2';
    update products set image='herbs(3).png', name='Lovage' where name like 'herb 3';
    update products set image='herbs(4).png', name='Dill' where name like 'herb 4';
    update products set image='vege(1).jpg', name='Rosa Lettuce' where name like 'vegetable 1';
    update products set image='vege(2).jpg', name='Rocket Lettuce' where name like 'vegetable 2';
    update products set image='vege(3).jpg', name='Broccolini' where name like 'vegetable 3';
    update products set image='vege(4).jpg', name='Spanish Frisee Lettuce' where name like 'vegetable 4';
    update products set image='edible_flower_1.jpg', name='begonia' where name like 'edible flower 1';
    update products set image='edible_flower_2.jpg', name='bachelor buttons' where name like 'edible flower 2';
    update products set image='edible_flower_3.jpg', name='borage flowers' where name like 'edible flower 3';
    update products set image='edible_flower_4.jpg', name='marigold' where name like 'edible flower 4';
`)
;

